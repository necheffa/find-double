#!/usr/bin/env bash

set -eu -o pipefail

export JAVA_HOME='/usr/local/java'
export ANT_HOME='/opt/java/apache-ant-1.10.12'

$ANT_HOME/bin/ant dist

