/*
     Copyright (C) 2015 Alexander Necheff

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, version 3 of the License.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <https://www.gnu.org/licenses/>
*/

package find_double;


//TODO: break off the fileMap put/replace functionality into a wrapper method

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;

public class FindDouble {
	public String version = "1.1";
	
	public ArrayList<ArrayList<String>> findDouble(String rootpath) {
		Map<String, ArrayList<String>> fileMap = new HashMap<String, ArrayList<String>>();
		Iterator<File> files = FileUtils.iterateFiles(new File(rootpath), FileFilterUtils.trueFileFilter(), FileFilterUtils.trueFileFilter());
		
		while (files.hasNext()) {
			File file = files.next();
			String checksum = null;
			try {
				/*
				  need to explicitly open a stream here so it can be closed explicitly
				  it seems implicit stream closing by wrapping the stream in a try block
				  does not work on anonymous streams
				  if we don't do this explicitly then file descriptors stay open for each stream
				  and the OS gets grumpy.
				*/
				FileInputStream stream = new FileInputStream(file);
				checksum = DigestUtils.md5Hex(stream);
				stream.close();
				
			} catch (IOException e) {
				//System.out.println("Unhandled IOException opening file stream: " + file.getAbsolutePath());
				//System.out.println("The exception was: " + e.getClass().getName());
				//System.out.println("Reason: " + e.getMessage());
				
				// TODO: refactor 
				if (null == fileMap.get("ERROR")) {
					ArrayList<String> list = new ArrayList<String>();
					list.add(e.getMessage());
					fileMap.put("ERROR", list);
				} else {
					ArrayList<String> oldList = fileMap.get("ERROR");
					oldList.add(e.getMessage());
					fileMap.replace("ERROR", oldList);
				}
				continue;
			}
			
			if (null == fileMap.get(checksum)) {
				ArrayList<String> list = new ArrayList<String>();
				list.add(file.getAbsolutePath());
				fileMap.put(checksum, list);
			} else {
				ArrayList<String> oldList = fileMap.get(checksum);
				oldList.add(file.getAbsolutePath());
				fileMap.replace(checksum, oldList);
			}
		}
		
		ArrayList<ArrayList<String>> paths = new ArrayList<ArrayList<String>>();
		Collection<ArrayList<String>> collection = fileMap.values();
		ArrayList<String> errors = fileMap.get("ERROR");
		
		for (ArrayList<String> list : collection) {
			if (list.size() > 1) {
				// if list size is greater than 1; the list contains duplicates
				paths.add(list);
			}
		}
		
		// make sure paths always has errors appended
		// regardless of errors.size()
		if (null != paths && null != errors) {
			paths.add(errors);
		}
		return paths;
	}	
}
